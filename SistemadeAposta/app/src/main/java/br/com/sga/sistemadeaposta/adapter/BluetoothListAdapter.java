package br.com.sga.sistemadeaposta.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.sga.sistemadeaposta.R;

/**
 * Created by Fabrício Sandes on 05/03/2017.
 */

public class BluetoothListAdapter extends ArrayAdapter<BluetoothDevice> {




    public BluetoothListAdapter(Context context, int resource, List<BluetoothDevice> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_bluetooth, null);
        }

        BluetoothDevice device = getItem(position);

        if (device != null) {
            TextView tvHorario = (TextView) v.findViewById(R.id.tvId);
            TextView tvDescricao = (TextView) v.findViewById(R.id.tvNome);

            tvHorario.setText(device.getAddress());
            tvDescricao.setText(device.getName());
        }


        return v;
    }


}
