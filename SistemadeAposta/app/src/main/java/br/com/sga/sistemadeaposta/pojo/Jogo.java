package br.com.sga.sistemadeaposta.pojo;


public class Jogo
{
    private String id;

    private String horario;

    private String casa;

    private String done;

    private String visitante;

    private String idcampeonato;

    private String odds;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getHorario ()
    {
        return horario;
    }

    public void setHorario (String horario)
    {
        this.horario = horario;
    }

    public String getCasa ()
    {
        return casa;
    }

    public void setCasa (String casa)
    {
        this.casa = casa;
    }

    public String getDone ()
    {
        return done;
    }

    public void setDone (String done)
    {
        this.done = done;
    }

    public String getVisitante ()
    {
        return visitante;
    }

    public void setVisitante (String visitante)
    {
        this.visitante = visitante;
    }

    public String getIdcampeonato ()
    {
        return idcampeonato;
    }

    public void setIdcampeonato (String idcampeonato)
    {
        this.idcampeonato = idcampeonato;
    }

    public String getOdds ()
    {
        return odds;
    }

    public void setOdds (String odds)
    {
        this.odds = odds;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", horario = "+horario+", casa = "+casa+", done = "+done+", visitante = "+visitante+", idcampeonato = "+idcampeonato+", odds = "+odds+"]";
    }
}

			
