package br.com.sga.sistemadeaposta.webutil;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.google.gson.Gson;

import br.com.sga.sistemadeaposta.R;
import br.com.sga.sistemadeaposta.activities.MainActivity;
import br.com.sga.sistemadeaposta.impressora.ConexaoImpressora;
import br.com.sga.sistemadeaposta.interfaces.RespostaConexaoImpressora;
import br.com.sga.sistemadeaposta.pojo.Aposta;

public class WebAppInterface   implements RespostaConexaoImpressora {

    private AlertDialog.Builder builder;

    private Context mContext;
    private ConexaoImpressora conexaoImpressora;
    private static ProgressDialog dialogConexaoImpressora;
    private String filaImpressao;


    /**
     * Instantiate the interfaces and set the context
     */
    public WebAppInterface(Context c) {
        mContext = c;

    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void imprimir(String dados) {
//        Log.i("JSON",dados);
//
//
//        Gson gson = new Gson();
//
//        Aposta aposta = gson.fromJson(dados,Aposta.class);
//
//        String bilhete = mContext.getResources().getString(R.string.titulo);
//
//        bilhete.replace("@identificador",aposta.getIdjogo());
//        bilhete.replace("@apostador", aposta.getApostador());
//        bilhete.replace("@cambista", aposta.getCambista());
//        bilhete.replace("@hora", aposta.getHorario());
//        Log.i("Bilhete",bilhete);
//
//        builder = new AlertDialog.Builder(mContext);
//        builder.setMessage(Html.fromHtml(bilhete).toString())
//                .setTitle(R.string.aguarde_titulo);
//
//        AlertDialog dialog = builder.create();
//        dialog.show();





        if (conexaoImpressora == null) {
            this.exibirAguardeConexaoImpressora();
            this.instanciaConexao();
            filaImpressao = Html.fromHtml(dados).toString();

        } else {
            filaImpressao = Html.fromHtml(dados).toString();
            imprimefilaImpressao();

        }


//        imprimefilaImpressao();


//        conexaoImpressora = ConexaoImpressora.getInstance(mContext,null);
//        conexaoImpressora.realizaImpressao(Html.fromHtml(dados).toString());
//        builder = new AlertDialog.Builder(mContext);
//        builder.setMessage(Html.fromHtml(dados).toString())
//                .setTitle(R.string.aguarde_titulo);
//
//        AlertDialog dialog = builder.create();
//        dialog.show();


    }



    private void exibirAguardeConexaoImpressora() {
        this.dialogConexaoImpressora = new ProgressDialog(mContext);
        dialogConexaoImpressora.setTitle(mContext.getResources().getString(R.string.aguarde_titulo));
        dialogConexaoImpressora.setMessage(mContext.getResources().getString(R.string.aguarde_conexaoimpressora));
        dialogConexaoImpressora.setIndeterminate(true);
        dialogConexaoImpressora.setCancelable(false);
        dialogConexaoImpressora.show();
    }

    private void instanciaConexao() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                WebAppInterface.this.conexaoImpressora = ConexaoImpressora.getInstance(mContext, WebAppInterface.this);
                return null;
            }
        }.execute(null, null, null);

    }

    @Override
    public void conexaRealizada(Boolean retornoConexao) {
        Toast.makeText(mContext,"Conexão realizada? " + retornoConexao,Toast.LENGTH_SHORT).show();
        this.dialogConexaoImpressora.dismiss();
        this.imprimefilaImpressao();

    }

    private void imprimefilaImpressao() {
//        conexaoImpressora = ConexaoImpressora.getInstance(mContext,null);
        try {
            conexaoImpressora.realizaImpressao(filaImpressao);
        } catch (Exception e) {
            Toast.makeText(mContext,e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
}