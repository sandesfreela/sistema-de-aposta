package br.com.sga.sistemadeaposta.interfaces;

/**
 * Created by Sandes on 08/03/2017.
 */

public interface RespostaConexaoImpressora {

    void conexaRealizada(Boolean retornoConexao);
}
