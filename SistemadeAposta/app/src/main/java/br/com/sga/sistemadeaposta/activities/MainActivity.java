package br.com.sga.sistemadeaposta.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import br.com.sga.sistemadeaposta.R;
import br.com.sga.sistemadeaposta.impressora.ConexaoImpressora;
import br.com.sga.sistemadeaposta.interfaces.RespostaConexaoImpressora;
import br.com.sga.sistemadeaposta.webutil.WebAppInterface;

public class MainActivity extends AppCompatActivity implements RespostaConexaoImpressora {

    private static ProgressDialog progressDialog;
    private static ProgressDialog dialogConexaoImpressora;
    private Context ctx;
    private Boolean clickSair = false;
//    private ConexaoImpressora conexaoImpressora;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.activity_web_view);
        progressDialog = new ProgressDialog(ctx);
        checkaPermissao();


        if (isConectado()) {


            WebView wb = (WebView) findViewById(R.id.wb);
            wb.getSettings().setJavaScriptEnabled(true);
            wb.getSettings().setDomStorageEnabled(true);
            wb.getSettings().setUseWideViewPort(false);
            wb.clearCache(Boolean.TRUE);
            wb.addJavascriptInterface(new WebAppInterface(this), "Android");

            wb.setWebViewClient(new WebViewClient() {

                @Override
                public void onReceivedSslError(WebView view,
                                               SslErrorHandler handler, SslError error) {
                    handler.proceed();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    progressDialog.dismiss();
                    super.onPageFinished(view, url);
//                    if (conexaoImpressora == null) {
//                        MainActivity.this.exibirAguardeConexaoImpressora();
//                        MainActivity.this.instanciaConexao();
//                    }

                }

                @Override
                public void onPageStarted(WebView view, String url,
                                          Bitmap favicon) {
                    progressDialog.dismiss();
                    exibirAguarde();
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onLoadResource(WebView view, String url) {
                    super.onLoadResource(view, url);
                }
            });


            wb.loadUrl(getResources().getString(R.string.url));


//			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(br.com.rjconsultores.aguiabranca.R.string.url)));
//			startActivity(browserIntent);
//			finish();
        } else {
            dialogSemConexao();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    private boolean isConectado() {
        try {
            ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()) {
                return Boolean.TRUE;
            } else if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()) {
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }

    private void sairAplicacao() {
        finish();
        System.exit(0);
    }

    private void exibirAguarde() {
        this.progressDialog = new ProgressDialog(ctx);
        progressDialog.setTitle(getResources().getString(R.string.aguarde_titulo));
        progressDialog.setMessage(getResources().getString(R.string.aguarde_mensagem));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void exibirAguardeConexaoImpressora() {
        this.dialogConexaoImpressora = new ProgressDialog(ctx);
        dialogConexaoImpressora.setTitle(getResources().getString(R.string.aguarde_titulo));
        dialogConexaoImpressora.setMessage(getResources().getString(R.string.aguarde_conexaoimpressora));
        dialogConexaoImpressora.setIndeterminate(true);
        dialogConexaoImpressora.setCancelable(false);
        dialogConexaoImpressora.show();
    }

    private void dialogSemConexao() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.semconexao_titulo));
        builder.setMessage(getResources().getString(R.string.semconexao_confirma))
                .setCancelable(false).setPositiveButton(getResources().getString(R.string.semconexao_confirma), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                Toast.makeText(ctx, getResources().getString(R.string.semconexao_toast), Toast.LENGTH_LONG).show();

            }
        }).setNegativeButton(getResources().getString(R.string.semconexao_cancela), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                sairAplicacao();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void checkaPermissao() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.BLUETOOTH_ADMIN)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.BLUETOOTH_ADMIN}, 1
            );

        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.BLUETOOTH)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.BLUETOOTH}, 3
            );

        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET}, 2
            );

        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2
            );

        }
    }

//    private void instanciaConexao() {
//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... params) {
//                MainActivity.this.conexaoImpressora = ConexaoImpressora.getInstance(MainActivity.this, MainActivity.this);
//                return null;
//            }
//        }.execute(null, null, null);
//
//    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isConectado()) {
            dialogSemConexao();
        }
    }

    @Override
    public void onBackPressed() {


        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                clickSair = false;

            }
        }).start();

        if (clickSair) {
            sairAplicacao();
        }
        Toast.makeText(ctx, getResources().getString(R.string.sairaplicacao_toast), Toast.LENGTH_SHORT).show();
        clickSair = true;

    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }


    @Override
    public void conexaRealizada(Boolean retornoConexao) {
        this.dialogConexaoImpressora.dismiss();
//        conexaoImpressora.printTest();

    }
}
