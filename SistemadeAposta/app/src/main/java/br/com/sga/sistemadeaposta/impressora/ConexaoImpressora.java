package br.com.sga.sistemadeaposta.impressora;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import com.datecs.api.printer.Printer;
import com.datecs.api.printer.ProtocolAdapter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import br.com.sga.sistemadeaposta.interfaces.RespostaConexaoImpressora;

/**
 * Created by Fabricio on 01/12/2015.
 */
public class ConexaoImpressora {

    private static BluetoothAdapter mBluetoothAdapter;

    private BroadcastReceiver discoveryResult;
    private BroadcastReceiver discoveryFinish;
    private BluetoothDevice mmDevice;
    private BluetoothSocket mmSocket;
    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private static ConexaoImpressora INSTANCE;
    private static Context CTX;
    //    private static final String PRINTER_NAME = "MTP-3";
//    private static final String PRINTER_NAME = "Mobile Printer";
    private static final String PRINTER_NAME = "DPP-350";
    private List<String> listPrinters;
    private static RespostaConexaoImpressora RESPOSTA_CONEXAO_IMPRESSORA;
    private Printer dppPrinter;


    private ConexaoImpressora() {
        listPrinters = new ArrayList<>();
        listPrinters.add("DPP-350");
        listPrinters.add("MTP-3");
        listPrinters.add("Mobile Printer");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        conectaBluetooth();
        startDiscovery();


    }

    public static ConexaoImpressora getInstance(Context ctx, RespostaConexaoImpressora respostaConexaoImpressora) {
        CTX = ctx;
        RESPOSTA_CONEXAO_IMPRESSORA = respostaConexaoImpressora;
        if (INSTANCE == null) {
            INSTANCE = new ConexaoImpressora();
        }

        return INSTANCE;
    }


    private void conectaBluetooth() {

        while (!mBluetoothAdapter.isEnabled()) {
            try {
                Log.d("ConexaoImpressora", "bluetooth ainda não habilitado");
                mBluetoothAdapter.enable();
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    private void startDiscovery() {


        discoveryResult = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("ConexaoImpressora", "Iniciando tentativa");
                BluetoothDevice remoteDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (remoteDevice != null) {
                    if (remoteDevice.getName() != null) {
                        for (String nome : listPrinters) {
                            if (remoteDevice.getName().equalsIgnoreCase(nome)) {
                                if (mmSocket == null || !mmSocket.isConnected()) {
                                    mmDevice = remoteDevice;
                                    if (!bluetoothOpenDevice()) {
                                        Toast.makeText(context, "Erro ao abrir conexão com impressora", Toast.LENGTH_SHORT).show();
                                    } else {

                                        if (remoteDevice.getName().equalsIgnoreCase("DPP-350")) {
                                            try {
                                                ProtocolAdapter mProtocolAdapter = new ProtocolAdapter(mmInputStream, mmOutputStream);
                                                ProtocolAdapter.Channel mPrinterChannel = mProtocolAdapter.getChannel(ProtocolAdapter.CHANNEL_PRINTER);
                                                dppPrinter = new Printer(mPrinterChannel.getInputStream(),mPrinterChannel.getOutputStream());
                                                ConexaoImpressora.RESPOSTA_CONEXAO_IMPRESSORA.conexaRealizada(Boolean.TRUE);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }

                                        } else {
                                            ConexaoImpressora.RESPOSTA_CONEXAO_IMPRESSORA.conexaRealizada(Boolean.TRUE);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };

        discoveryFinish = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("ConexaoImpressora", "Discovery finalizou sem resultado");

            }
        };

        Log.d("ConexaoImpressora", "registrar filtro 1");
        CTX.registerReceiver(discoveryFinish, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
        Log.d("ConexaoImpressora", "registrar filtro 2");
        CTX.registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        Log.d("ConexaoImpressora", "iniciar discovery");
        mBluetoothAdapter.startDiscovery();


    }

    public boolean isConnected() {
        return this.mmSocket.isConnected();
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        CTX.unregisterReceiver(discoveryResult);
        CTX.unregisterReceiver(discoveryFinish);
    }


    boolean bluetoothOpenDevice() {
        try {
            mBluetoothAdapter.cancelDiscovery();
//            mmSocket = mmDevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
            return true;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public void printTest() {
        DataOutputStream out = new DataOutputStream(mmOutputStream);
        byte[] dataToSend = "1234567890ABCDEFGHIJKLMNOPQRSTUVXWYZ1234567890ABCDEFGHIJKLMNOPQRSTUVXWYZ".getBytes();
        try {
            out.writeInt(dataToSend.length);

            out.write(dataToSend, 0, dataToSend.length);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void realizaImpressao(String dados) {
        try {
            if (dppPrinter != null) {
//                Toast.makeText(CTX, "DPP PRINTER", Toast.LENGTH_SHORT).show();
                dppPrinter.reset();
                dppPrinter.printText(dados);
                dppPrinter.feedPaper(30);
                dppPrinter.flush();

            } else {

                DataOutputStream out = new DataOutputStream(mmOutputStream);
                byte[] dataToSend = dados.getBytes();

                out.writeInt(dataToSend.length);

                out.write(dataToSend, 0, dataToSend.length);
                out.flush();

            }
        } catch (Exception e) {
            Toast.makeText(CTX, "Erro ao imprimir :" + e.getMessage(), Toast.LENGTH_SHORT).show();
            AlertDialog.Builder alert = new AlertDialog.Builder(CTX);
            e.printStackTrace();
        }
    }

}
