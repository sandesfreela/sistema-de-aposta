package br.com.sga.sistemadeaposta.pojo;

public class Aposta
{
    private String horario;

    private Jogo[] jogos;

    private String apostador;

    private String data;

    private String premio;

    private String opt_aposta;

    private String cambista;

    private String odds;

    private String comissao;

    private String idjogo;

    private String valor;

    private String idapostador;

    private String idcambista;

    private String vencedor;

    public String getHorario ()
    {
        return horario;
    }

    public void setHorario (String horario)
    {
        this.horario = horario;
    }

    public Jogo[] getJogos ()
    {
        return jogos;
    }

    public void setJogos (Jogo[] jogos)
    {
        this.jogos = jogos;
    }

    public String getApostador ()
    {
        return apostador;
    }

    public void setApostador (String apostador)
    {
        this.apostador = apostador;
    }

    public String getData ()
    {
        return data;
    }

    public void setData (String data)
    {
        this.data = data;
    }

    public String getPremio ()
    {
        return premio;
    }

    public void setPremio (String premio)
    {
        this.premio = premio;
    }

    public String getOpt_aposta ()
    {
        return opt_aposta;
    }

    public void setOpt_aposta (String opt_aposta)
    {
        this.opt_aposta = opt_aposta;
    }

    public String getCambista ()
    {
        return cambista;
    }

    public void setCambista (String cambista)
    {
        this.cambista = cambista;
    }

    public String getOdds ()
    {
        return odds;
    }

    public void setOdds (String odds)
    {
        this.odds = odds;
    }

    public String getComissao ()
    {
        return comissao;
    }

    public void setComissao (String comissao)
    {
        this.comissao = comissao;
    }

    public String getIdjogo ()
    {
        return idjogo;
    }

    public void setIdjogo (String idjogo)
    {
        this.idjogo = idjogo;
    }

    public String getValor ()
    {
        return valor;
    }

    public void setValor (String valor)
    {
        this.valor = valor;
    }

    public String getIdapostador ()
    {
        return idapostador;
    }

    public void setIdapostador (String idapostador)
    {
        this.idapostador = idapostador;
    }

    public String getIdcambista ()
    {
        return idcambista;
    }

    public void setIdcambista (String idcambista)
    {
        this.idcambista = idcambista;
    }

    public String getVencedor ()
    {
        return vencedor;
    }

    public void setVencedor (String vencedor)
    {
        this.vencedor = vencedor;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [horario = "+horario+", jogos = "+jogos+", apostador = "+apostador+", data = "+data+", premio = "+premio+", opt_aposta = "+opt_aposta+", cambista = "+cambista+", odds = "+odds+", comissao = "+comissao+", idjogo = "+idjogo+", valor = "+valor+", idapostador = "+idapostador+", idcambista = "+idcambista+", vencedor = "+vencedor+"]";
    }
}
